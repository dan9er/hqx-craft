# Contributing guidelines
### Please use [hqxSharp](https://bitbucket.org/Tamschi/hqxsharp).
This is an updated fork of hqx with transparency support. It also provides a GUI. You are deprecated from using the original hqx.

### You may fork to contribute.
This includes adding textures or fixing a mistake.

### There should be as little editing of the filter output as possible.
Editing should only be used to fix:
- Translucent edges
- "Dead pixels"
- Misalignments when the texture is tiled

### The filter should be set to default settings.
This to maintain consistency between all textures.

### Use the templates for issues and pull requests (but not questions or suggestions).
If you don't use these templates there is a high chance your issue will be insta-closed/pull request will be denied. **See below for the templates.**

### Before opening an issue, search to make sure your issue doesn't already exist.
You can do this by removing the *is:open* in the search bar.

### Make sure to test your textures before submitting a push request.
You can download the pack from the repository to your computer by:
1. Clicking *Clone or download* -> *Download ZIP*
2. Unzipping the folder
3. Taking the *2x* and/or *4x* folder inside a version folder (*MC1XX*) and putting it in your *.minecraft/resourcepacks* folder
4. Open Minecraft and activate the resource pack

---

## Templates
### Issue
#### *You do NOT need to follow this template if you are just asking a question or making a suggestion!*

- **(Title):** The issue title should be a brief description of the problem (eg: *Diamond Sword has translucent edges*)

- **Texture/File Affected:** Name which texture (or file) has a problem
- **Problem:** Describe the problem
- **Screenshots:** Add screenshots of the texture in question in-game if you can
- **Additional Notes:** Anything else should we know?

```
**Texture/File Affected:** 
**Problem:** 
**Screenshots:** 
**Addtional Notes:** 
```

### Pull Request
- **Textures/Files Changed/Added (Title):** Self explanatory. This can also be your title
- **Screenshots:** Add screenshots of the textures you added/changed
- **Additional Notes:** Anything else we should know?

```
**Textures/Files Changed/Added:** 
**Screenshots:** 
**Addtional Notes:** 
```
