# hqxCraft ![language](https://img.shields.io/badge/language-GFX%20Custom-green.svg)
A resource pack for Minecraft with all textures put through the [hqx]( https://en.wikipedia.org/wiki/Hqx) filter with as little modification as possible.
### This project is currently on hold until further notice

## Contributing guidelines in a nutshell

- Please use [hqxSharp](https://bitbucket.org/Tamschi/hqxsharp).
- You may fork to contribute.
- There should be as little editing of the filter output as possible.
- The filter should be set to default settings.
- Use the templates for issues and pull requests (but not questions or suggestions).
- Before opening an issue, search to make sure your issue doesn't already exist.
- Make sure to test your textures before submitting a push request.

**For more details and issue/pull request templates, read the [CONTRIBUTING.md](CONTRIBUTING.md) file.**

## License [![license](https://img.shields.io/badge/license-CC%20BY--NC--SA%204.0-blue.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

hq2xCraft & hq4xCraft are licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/). The preceding link is a human readable summary. A copy of the license in Legalese can be found in [LICENSE.txt](LICENSE.txt).
